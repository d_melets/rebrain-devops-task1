# Почему NGINX?
![](https://media-exp1.licdn.com/dms/image/C4E0BAQFVNia9avQ6aQ/company-logo_200_200/0/1570661875266?e=2159024400&v=beta&t=kPTDQ9rkzAq2-GZFDWhKYVN5XvaJUrHypJfmTpaakok)
### Основные преимущества, сравнение с Apache:
Целесообразность выбора того или иного решения для веб-сервера определяется на основе следующих критериев:

* Производительность. Nginx демонстрирует высокую скорость обработки подключений статического контента. По этому показателю он обходит ближайшего конкурента (Apache) в 2 раза. Производительность при работе с динамическими сайтами у обоих программных продуктов примерно одинаковая.
* Использование ресурсов. Nginx является менее требовательным к памяти, чем веб-сервер Apache.
* Совместимость с ОС. Nginx поддерживает многие популярные операционные системы. Однако, он разрабатывался для UNIX-систем. Совместимость с Windows реализована слабо, поэтому скорость работы ПО в этой системе достаточно низкая.
* Пользовательская поддержка. Помощь клиентам предоставляется в рамках e-mail-переписки. Также существуют форумы сообществ, на которых обсуждают различные вопросы.

### Основные отличия от Apache:

1. Быстрая обработка запросов, связанных со статическим контентом. Однако, Nginx не содержит алгоритмов для самостоятельной обработки запросов к динамическим данным. Для этого используется внешний процессор, который выполняет функции по обработке и возвращает итоговый результат Nginx. Последний в свою очередь отправляет его клиенту.
2. Запрет на возможность переопределения конфигурационных файлов на уровне директорий, обуславливающий прирост производительности и безопасности в сравнении с Apache, интерпретирующий каждый .htaccess-файл.
3. Ориентир на работу с URI в первую очередь, которые только при необходимости транслируются в запросы к файловой системе. Такой подход обеспечивает совмещение двух основных функциональностей: прокси-сервер и веб-сервер.
4. Отсутствие механизма динамического подключения различных модулей (для шифрования, проксирования, почтовых функций и прочие). С этим связаны как преимущества (безопасность, подключение только необходимых модулей), так и недостатки (необходимость ручной сборки, более низкая гибкость решения в сравнении с Apache).
5. Высокая масштабируемость при более низких требованиях к вычислительным системам (физическим серверам).

### Сравнительная таблица:
 
|Особенность|Apache|Nginx|
| ------------- | ------------- |------------- |
| Простота | Легко разрабатывать и внедрять инновации благодаря своей модели «одно соединение на процесс» | Сложный в разработке, поскольку он имеет сложную архитектуру для одновременной обработки нескольких соединений.  |
| Производительность - Статический контент | Медленно в отображении статического контента  | В 2,5 раза быстрее чем Apache и потребляет меньше памяти|
| Производительность - Динамический контент | Отличная производительность для динамического контента  | Отличная производительность для динамического контента|
|Гибкость|Можно настроить, добавив модули. Apache имел динамическую загрузку модулей дольше всего.|Nginx версии 1.11.5 и Nginx Plus Release R11 представили совместимость для динамических модулей.|

### Basic configuration

```
daemon            off;
worker_processes  2;
user              www-data;

events {
    use           epoll;
    worker_connections  128;
}

error_log         logs/error.log info;

http {
    server_tokens off;
    include       mime.types;
    charset       utf-8;

    access_log    logs/access.log  combined;

    server {
        server_name   localhost;
        listen        127.0.0.1:80;

        error_page    500 502 503 504  /50x.html;

        location      / {
            root      html;
        }

    }

}
```

Более подробно можно ознакомиться [по ссылке](https://ru.wikipedia.org/wiki/Nginx).


## P.S.
>«Интеллект — это способность избегать выполнения работы, но так, чтобы она
>при этом была сделана.»
